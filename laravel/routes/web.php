<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use App\Post;
use App\Category;
use App\Http\Resources\UserResource;
use App\Http\Resources\PostResource;

Route::resource('/category', 'CategoryController');
Route::resource('/user_detail','UserDetailController');
Route::resource('/post','PostController');
Route::get('/','PostController@index');
Route::get('/my_post','PostController@myPost');

//ajax routes
Route::post('/post/filter', 'PostController@getFilterPost')->name('post.filter');


Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/json',function(){
  return new PostResource(Post::all());
});
