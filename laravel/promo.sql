-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 08:48 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

drop
if exists promo;
use

SET SQL_MODE
= "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT
= 0;
START TRANSACTION;
SET time_zone
= "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `promo`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories`
(
  `id` int
(10) UNSIGNED NOT NULL,
  `name` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`
id`,
`name
`, `created_at`, `updated_at`) VALUES
(1, 'Foods', '2018-12-17 14:44:53', '2018-12-17 14:59:01'),
(2, 'Drink', '2018-12-17 14:45:04', '2018-12-17 14:45:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations`
(
  `id` int
(10) UNSIGNED NOT NULL,
  `migration` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int
(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`
id`,
`migration
`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_14_101638_create_categories_table', 1),
(4, '2018_12_14_101789_create_user_details_table', 1),
(5, '2018_12_14_102143_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets`
(
  `email` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts`
(
  `id` int
(10) UNSIGNED NOT NULL,
  `user_detail_id` int
(10) UNSIGNED NOT NULL,
  `category_id` int
(10) UNSIGNED NOT NULL,
  `title` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`
id`,
`user_detail_id
`, `category_id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Testing Dish', '1545078083_me-dribbble-size-001-001_1x.png', '<p>This is the testing content</p>', '2018-12-17 15:21:23', '2018-12-17 15:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users`
(
  `id` int
(10) UNSIGNED NOT NULL,
  `name` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar
(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`
id`,
`name
`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'adil', 'adil@gmail.com', NULL, '$2y$10$oDlWlJKvnKNc8tix7zC2CO/kfQ64spqC5u6AdDCsV8a.XBw1AYT32', NULL, '2018-12-17 14:04:38', '2018-12-17 14:04:38');


-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details`
(
  `id` int
(10) UNSIGNED NOT NULL,
  `user_id` int
(10) UNSIGNED NOT NULL,
  `shop` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar
(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`
id`,
`user_id
`, `shop`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 1, 'Test Shop', '+923006669983', 'faisalabad, Pakistan', '2018-12-17 14:11:47', '2018-12-17 14:11:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
ADD KEY `password_resets_email_index`
(`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
ADD PRIMARY KEY
(`id`),
ADD KEY `posts_user_detail_id_foreign`
(`user_detail_id`),
ADD KEY `posts_category_id_foreign`
(`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
ADD PRIMARY KEY
(`id`),
ADD UNIQUE KEY `users_email_unique`
(`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
ADD PRIMARY KEY
(`id`),
ADD KEY `user_details_user_id_foreign`
(`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int
(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int
(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int
(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int
(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int
(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY
(`category_id`) REFERENCES `categories`
(`id`) ON
DELETE CASCADE ON
UPDATE CASCADE,
ADD CONSTRAINT `posts_user_detail_id_foreign` FOREIGN KEY
(`user_detail_id`) REFERENCES `user_details`
(`id`) ON
DELETE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
ADD CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY
(`user_id`) REFERENCES `users`
(`id`) ON
DELETE CASCADE ON
UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
