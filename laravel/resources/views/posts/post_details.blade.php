@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
        
            <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                    <img  class="card-img-top" src="{{ Storage::url('avatars/'.$post->image) }}" alt="Card image cap">
                    <a>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
            </div>
        </div> 
        <div class="col-md-6">
            <div class="card card-cascade narrower">
                <!-- Card content -->
                <div class="card-body card-body-cascade">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Title -->
                                <h4 class="font-weight-bold card-title">{{$post->title}}</h4>
                                <!-- Text -->
                                <p class="card-text">{!! $post->content !!}</p>
                                <p class="card-text"><strong>Date: </strong> {{ $post->created_at }}</p> <br>
                            </div>
                            <div class="col-md-6">
                                <h4 class="font-weight-bold card-title">Shop Details</h4>
                                <p class="card-text"><strong>Phone: </strong> {{ $user_detail->phone }}</p>
                                <p class="card-text"><strong>Shop: </strong> {{ $user_detail->shop }}</p>
                                <p class="card-text"><strong>Address: </strong> {{ $user_detail->address }}</p>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="/"><button class="btn btn-primary">Back</button></a>
                            </div>
                            <div class="col-md-6">
                                @if( $user_detail->user_id == auth()->user()->id)
                                    <div style="float: right" class="btn-group">
                                        <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ route('post.edit', $post->id) }}">Edit</a>
                                            {!!Form::open(['action' => ['PostController@destroy', $post->id ], 'method' => 'POST'])!!}
                                                {{ Form::hidden('_method','DELETE') }}  
                                                {{ Form::submit('Delete',['class'=>'dropdown-item'])}} 
                                            {!!Form::close()!!}
                                        </div>
                                    </div>  
                                @endif
                            </div>
                            
                        </div>
                    </div>
            </div>
        </div> 
    </div> 

@endsection