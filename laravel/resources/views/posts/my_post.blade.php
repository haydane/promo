@extends('layouts.app')
@section('content')

    <!-- Card Narrower -->
    <br><br><br>
    <div class="row" id="post-wrapper">
        @if(!empty($posts))
            @foreach($posts as $post)
                @if($post->user_detail_id == auth()->user()->id)
                  <a href="/post/ {{ $post->id }}">
                      <div class="col-md-4" style="margin-top: 20px;">
                          <div class="card card-cascade narrower">

                              <!-- Card image -->
                              <div class="view view-cascade overlay">
                                  <img style="height:300px" class="card-img-top" src="{{ Storage::url('avatars/'.$post->image) }}"
                                      alt="Card image cap">
                                  <a>
                                      <div class="mask rgba-white-slight"></div>
                                  </a>
                              </div>

                              <!-- Card content -->
                              <div class="card-body card-body-cascade">
                                  <h4 style="text-align: center;" class="font-weight-bold card-title">{{$post->title}}</h4>
                              </div>
                          </div>
                      </div>
                  </a>
                @endif
            @endforeach
        @endif

    </div>
    <!-- Card Narrower -->

@endsection