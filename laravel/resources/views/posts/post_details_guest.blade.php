@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
        
            <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                    <img  class="card-img-top" src="{{ Storage::url('avatars/'.$post->image) }}" alt="Card image cap">
                    <a>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
            </div>
        </div> 
        <div class="col-md-6">
            <div class="card card-cascade narrower">
                <!-- Card content -->
                <div class="card-body card-body-cascade">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Title -->
                                <h4 class="font-weight-bold card-title">{{$post->title}}</h4>
                                <!-- Text -->
                                <p class="card-text">{!! $post->content !!}</p>
                                <p class="card-text"><strong>Date: </strong> {{ $post->created_at }}</p>
                            </div>
                            <div class="col-md-6">
                                <h4 class="font-weight-bold card-title">Shop Details</h4>
                                <p class="card-text"><strong>Phone: </strong> {{ $user_detail->phone }}</p>
                                <p class="card-text"><strong>Shop: </strong> {{ $user_detail->shop }}</p>
                                <p class="card-text"><strong>Address: </strong> {{ $user_detail->address }}</p>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="/"><button class="btn btn-primary">Back</button></a>
                            </div>
                        </div>
                    </div>
            </div>
        </div> 
    </div> 

@endsection