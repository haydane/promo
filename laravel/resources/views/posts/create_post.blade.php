@extends('layouts.app')
@section('content')
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{__('Create Post')}}</div>
        <div class="card-body">
          {!! Form::open(['action' => 'PostController@store', 'method' => 'POST', 'files'=> true, 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
              {{ Form::label('cat_name','Choose Category') }}
              <select class="form-control" name="category_id" id="">
                  @if(!empty($categories))
                      @foreach($categories as $cat)
                          <option @if($cat->name == 'All') disabled @endif value={{$cat->id}}>{{$cat->name}}</option>
                      @endforeach
                  @endif
              </select>
            </div>

            <div class="form-group">
              {{ Form::label('title','Title') }}
              {{ Form::text('title','',['class'=>'form-control']) }}
            </div>

            <div class="form-group">
              {{ Form::label('image',' Image') }}
              {{ Form::file('image',['class'=>'form-control']) }}
            </div>

            <div class="form-group">
              {{ Form::label('content','Content') }}
              {{ Form::textArea('content','',['id' => 'article-ckeditor','class'=>'form-control']) }}
            </div>

            {!! Form::submit('Post',['class'=>'btn btn-primary']) !!}

          {!! Form::close() !!}
        </div>  
      </div>
    </div>
  </div>
@endsection