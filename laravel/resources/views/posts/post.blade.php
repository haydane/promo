@extends('layouts.app')
@section('content')

    <select style="float:right;width: 100px" class="form-control" name="category_id" id="category">
    <!-- for all never store all in database :) -->
        <option value="0">All</option>
        @if(!empty($categories))    
            @foreach($categories as $cat)
                <option value={{$cat->id}}>{{$cat->name}}</option>
            @endforeach
        @endif
    </select>
    <span style="float: right; margin: 8px">sort by:</span>
    <!-- Card Narrower -->
    <br><br><br>
    <div class="row" id="post-wrapper">
        @if(!empty($posts))
            @foreach($posts as $post)
                <a href="/post/ {{ $post->id }}">
                    <div class="col-md-4" style="margin-top: 20px;">
                        <div class="card card-cascade narrower">

                            <!-- Card image -->
                            <div class="view view-cascade overlay">
                                <img style="height:300px" class="card-img-top" src="{{ Storage::url('avatars/'.$post->image) }}"
                                    alt="Card image cap">
                                <a>
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>

                            <!-- Card content -->
                            <div class="card-body card-body-cascade">

                                <!-- Label -->
                                <h5 class="pink-text pb-2 pt-1"></i>{{$post->shop}}</h5>
                                <!-- Title -->
                                <h4 style="text-align: center;" class="font-weight-bold card-title">{{$post->title}}</h4>
                                <!-- Text -->
                                <!-- <p class="card-text">{!! $post->getShortContentAttribute() !!}</p> -->
                                <!-- Button -->
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        @endif



    </div><br>
    <!-- Card Narrower -->
    {{ $posts->links() }}
    <br>

@endsection

@push('js')
    <script>
    $("#category").change( function(){
        //getting the category id
        var category_id = $(this).val();
        //getting the csrf tokken for post request
        var token = $('meta[name=csrf-token]').attr("content");
        $.ajax({
            url : '{{route('post.filter')}}',
            type: 'POST',
            //sending token and category_id in post request
            data: {_token: token, category_id: category_id},
            //send the json response
            dataType: 'JSON',
            //get the response in the data
            success : function(data){
                var posts = '';
                if(data.status == 'success'){
                    $(data.data).each( function(i,d){
                        posts += '<a href="/post/'+ d.id +'">\n' +
                                    '<div class="col-md-4" style="margin-top: 20px;">\n'+
                                        '<div class="card card-cascade narrower">\n'+
                                            '<div class="view view-cascade overlay">\n'+
                                                '<img style="height:300px" class="card-img-top" src="{{Storage::url('avatars/')}}'+ d.image +'"\n'+
                                                   ' alt="Card image cap">\n'+
                                                '<a>\n'+
                                                    '<div class="mask rgba-white-slight"></div>\n'+
                                                '</a>\n'+
                                            '</div>\n'+
                                            '<div class="card-body card-body-cascade">\n'+
                                                '<h5 class="pink-text pb-2 pt-1"><i class="fa fa-cutlery"></i></h5>\n'+
                                                '<h4 style="text-align: center;" class="font-weight-bold card-title">'+ d.title +'</h4>\n'+
                                            '</div>\n'+
                                        '</div>\n'+
                                    '</div>\n'+
                                '</a>';
                    });
                    $("#post-wrapper").html(posts);
                }
                // error: function(data){
                //     if(data.status == 'fail'){
                //         post += '<h3 class="center-text">Sorry No post available against this category.</h3>';
                //         $("#post-wrapper").html(post);
                //     }
                // }
            }
        });
    });
    </script>
@endpush