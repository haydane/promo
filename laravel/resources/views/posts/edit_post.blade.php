@extends('layouts.app')
@section('content')
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{__('Edit Post')}}</div>
        <div class="card-body">
          {!! Form::open(['action' => ['PostController@update', $post->id], 'method' => 'POST', 'files'=> true, 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
              {{ Form::label('cat_name','Choose Category') }}
              <select class="form-control" name="category_id" >
                  @foreach($cats as $cat)
                   <option value="{{$cat->id}}" @if($cat->id == $post->category_id) selected @endif>{{$cat->name}}</</option>
                  @endforeach
              </select>
            </div>

            <div class="form-group">
              {{ Form::label('title','Title') }}
              {{ Form::text('title',$post->title,['class'=>'form-control']) }}
            </div>

            <div class="form-group">
              {{ Form::label('image',' Image') }}
              {{ Form::file('image' ,['class'=>'form-control']) }}
            </div>

            <div class="form-group">
              {{ Form::label('content','Content') }}
              {{ Form::textArea('content',$post->content,['id' => 'article-ckeditor','class'=>'form-control']) }}
            </div>
            {!! Form::hidden('_method','PUT')!!}
            {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}
            <a style="float: right" class="btn btn-primary" href="/">Back</a>
          {!! Form::close() !!}
        </div>  
      </div>
    </div>
  </div>
@endsection