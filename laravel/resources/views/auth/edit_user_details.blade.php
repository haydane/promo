@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Details') }}</div>
                  <div class="card-body">
                  {!! Form::open(['action'=> ['UserDetailController@update', $user_detail->id],'method'=>'POST']) !!}
                    <div class="form-group">
                      {{Form::label('shop','Shop')}}
                      {{Form::text('shop',$user_detail->shop, ['class'=>'form-control', 'placeholder' => 'shop'])}}
                    </div>
                    <div class="form-group">
                      {{Form::label('phone','Phone')}}
                      {{Form::text('phone',$user_detail->phone, ['class'=>'form-control', 'placeholder' => 'Phone'])}}
                    </div>
                    <div class="form-group">
                      {{Form::label('address','Address')}}
                      {{Form::text('address',$user_detail->address, ['class'=>'form-control', 'placeholder' => 'Address'])}}
                    </div>
                    {!! Form::hidden('_method','PUT')!!}
                    {!! Form::submit('Update',['class' =>  'btn btn-primary']) !!}
                    <a style="float: right" class="btn btn-primary" href="/">Back</a>
                  {!! Form::close() !!}
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

