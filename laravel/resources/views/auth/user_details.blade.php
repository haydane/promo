@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details') }}</div>
                  <div class="card-body">
                  {!! Form::open(['action'=>'UserDetailController@store','method'=>'POST']) !!}
                    <div class="form-group">
                      {{Form::label('shop','Shop')}}
                      {{Form::text('shop','', ['class'=>'form-control', 'placeholder' => 'Shop'])}}
                    </div>
                    <div class="form-group">
                      {{Form::label('phone','Phone')}}
                      {{Form::text('phone','', ['class'=>'form-control', 'placeholder' => 'Phone'])}}
                    </div>
                    <div class="form-group">
                      {{Form::label('address','Address')}}
                      {{Form::text('address','', ['class'=>'form-control', 'placeholder' => 'Address'])}}
                    </div>
                    {!! Form::submit('Submit',['class' =>  'btn btn-primary']) !!}
                  {!! Form::close() !!}
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

