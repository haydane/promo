<div class="row">
  @if(session()->has('msg'))
    <div class="alert alert-success alert-dismissible col-md-12" role="alert" style="display: inline-block;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <ul style="list-style: none;">
        <li>{{ session()->get('msg') }}</li>
      </ul>
    </div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible col-md-12" role="alert" style="display: inline-block;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <ul style="list-style: none;">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
</div>