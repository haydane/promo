@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Create Post')}}</div>
                <div class="card-body">
                    <form action="{{ route('category.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label>Category</label>
                            <input class="form-control" name="category_name" type="text">
                        </div>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection