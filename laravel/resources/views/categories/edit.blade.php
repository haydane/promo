@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Edit Category')}}</div>
                <div class="card-body">
                    <form action="{{ route('category.update', $category->id) }}" method="post">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label>Category</label>
                            <input class="form-control" name="category_name" type="text" value="{{ $category->name }}">
                        </div>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection