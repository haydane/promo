@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Edit Category')}}</div>
                <div class="card-body">
                    @if(!empty($categories))
                        <table class="table table-hover">
                            <thead>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->name}}</td>
                                        <td><a class="btn btn-sm btn-success" href="{{ route('category.edit', $category->id) }}">Edit</a></td>
                                        <td>
                                            <form action="{{ route('category.destroy', $category->id) }}" method="post">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-sm btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        Nothing found
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection