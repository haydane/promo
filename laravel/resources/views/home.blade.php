@extends('layouts.app')
@section('content')

    <select style="float:right;width: 100px" class="form-control" name="category_id" id="">
        @if(!empty($categories))
            @foreach($categories as $cat)
                <option value={{$cat->id}}>{{$cat->name}}</option>
            @endforeach
        @endif
    </select>
    <span style="float: right; margin: 8px">sort by:</span>
    <!-- Card Narrower -->
    <br><br><br>
    <div class="row">
        @if(!empty($posts))
            @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card card-cascade narrower">

                        <!-- Card image -->
                        <div class="view view-cascade overlay">
                            <img class="card-img-top" src="{{ Storage::url('avatars/'.$post->image) }}"
                                 alt="Card image cap">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body card-body-cascade">

                            <!-- Label -->
                            <h5 class="pink-text pb-2 pt-1"><i class="fa fa-cutlery"></i>{{$post->shop}}</h5>
                            <!-- Title -->
                            <h4 class="font-weight-bold card-title">{{$post->title}}</h4>
                            <!-- Text -->
                            <p class="card-text">{!! $post->content !!}</p>
                            <!-- Button -->
                            <a>
                                <a href='/post/{{ $post->id }}' class="btn btn-primary">Details</a>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        

    </div>
    <!-- Card Narrower -->

@endsection