<?php

namespace App\Http\Controllers;

use App\Category;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $posts = Post::with('user_detail')->orderBy('created_at','desc')->paginate(9);
        return view('posts.post', compact('categories','posts'));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('posts.create_post', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'mimes:png,jpg,svg,jpeg',
            'content'=>'required',
            ]);

        //uploading the image locally in the folder, you can change it
            $imageName = null;
            if( $request->hasFile('image')){
                $imageName = $request->file('image')->getClientOriginalName();
                //concating the image name with time-stamp so every-image have unique name in folder
                $imageName = time().'_'.$imageName;

                $path = Storage::disk('public')->putFileAs(
                    'avatars', $request->file('image'), $imageName
                );
            }

        Post::create([
           'user_detail_id' => Auth()->user()->id,
            'category_id'   => $request->category_id,
            'title'         => $request->title,
            'image'         => $imageName,
            'content'       => $request->content
        ]);

        return redirect('/')->with('msg', 'Post has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::all();
        //use find or fail because if it does not id it will make an exception 
        //and doesnot exceute code further
        $post = Post::findOrFail($id);
        //getting with relationship
        $user_detail = $post->user_detail;
        // $user_detail = UserDetail::findOrFail($id);
        if(Auth::guest())
        {
            return view('posts.post_details_guest', compact('categories','post','user_detail'));
        }
        else
        {
            return view('posts.post_details', compact('categories','post','user_detail'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $categories = Category::all();
        $cats = Category::all();
        $post = Post::findOrFail($id);
        if($post->user_detail->user_id == Auth()->user()->id){
            return view('posts.edit_post', compact('post','cats'));
        }else{
            return redirect()->back()->with('msg', 'You can not edit this post');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=> 'mimes:png,jpg,svg,jpeg',
            'content'=>'required',
            ]);

        $post = Post::findOrFail($id);
        if($post->user_detail->user_id == auth()->user()->id){
            //uploading the image locally in the folder, you can change it
                
                if( $request->hasFile('image')){
                    $imageName = $request->file('image')->getClientOriginalName();
                    //concating the image name with time-stamp so every-image have unique name in folder
                    $imageName = time().'_'.$imageName;

                    $path = Storage::disk('public')->putFileAs(
                        'avatars', $request->file('image'), $imageName
                    );
                }else{
                    $imageName = $post->image;
                }

            //in case you pass category name from form then
            // $category = Category::where('name', $request->category_name)->get();   
            // $cat_id = $category->id; 

            $post->update([
            'user_detail_id' => Auth()->user()->id,
                'category_id'   => $request->category_id,
                'title'         => $request->title,
                'image'         => $imageName,
                'content'       => $request->content
            ]);

            return redirect('/')->with('msg', 'Product has been Updated.');
        }else{
            return back()->with('msg', 'You can not edit this post.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect()->route('post.index')->with('msg', 'Post has been deleted.');
    }

    public function getFilterPost(Request $request){
        $cat_id = $request->category_id;
        if($cat_id == 0){
            $posts = Post::all();
            if(!empty($posts)){
                return response()->json(['data'=>$posts, 'status'=>'success']);
            }else{
                return response()->json(['status'=>'fail']);
            }
        }else{
            $posts = Post::with('category')->where('category_id', $cat_id)->get();
            if(!empty($posts)){
                return response()->json(['data'=>$posts, 'status'=>'success']);
            }else{
                return response()->json(['status'=>'fail']);
            }
        }
        
    }
    public function myPost()
    {
        // $categories = Category::all();
        // $posts = \DB::table('posts')
        //                             ->join('user_details', 'posts.user_detail_id', '=', 'user_details.id')
        //                             ->join('users','users.id','=', 'user_details.user_id')
        //                             ->select('posts.*','user_details.*')
        //                             ->get();
        $posts = Post::with('user_detail')->orderBy('created_at','desc')->get();
        return view('posts.my_post', compact('posts'));
    }
}