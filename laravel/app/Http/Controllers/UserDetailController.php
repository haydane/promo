<?php

namespace App\Http\Controllers;

use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.user_details');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'shop'=>'required',
            'phone'=>'required',
            'address'=>'required'
        ]);
        //create user details
        UserDetail::create([
           'user_id' => Auth()->user()->id,
           'shop'    => $request->shop,
           'phone'   => $request->phone,
           'address' => $request->address
        ]);
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_detail = UserDetail::findOrFail($id);
        return view('auth.edit_user_details',compact('user_detail'));
    }   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'shop'=>'required',
            'phone'=>'required',
            'address'=>'required'
        ]);
        
        //update user details
        $user_detail = UserDetail::findOrFail($id);
        $user_detail->update([
           'user_id' => Auth()->user()->id,
           'shop'    => $request->shop,
           'phone'   => $request->phone,
           'address' => $request->address
        ]);
        return redirect('/home')->with('msg','user_detail has been updated');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
