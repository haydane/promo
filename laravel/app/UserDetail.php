<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_details';
    protected $guarded = [];

    public function posts(){
        return $this->hasMany(Post::class, 'user_detail_id', 'id');
    }
}
