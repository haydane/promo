<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'user_detail_id', 'category_id', 'title', 'image', 'content'
    ];

    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function user_detail(){
        return $this->belongsTo(UserDetail::class, 'user_detail_id', 'id');
    }  
    
    public function getShortContentAttribute()
    {
        return substr($this->content, random_int(0,100)) . '...';
    }
}

